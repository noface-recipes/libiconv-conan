import os, shutil
from os import path

from conans import ConanFile, tools
from conans.tools import download, unzip, replace_in_file, check_md5
from conans import AutoToolsBuildEnvironment

class LibiconvConan(ConanFile):
    name = "libiconv"
    version = "1.14"
    ZIP_FOLDER_NAME = "libiconv-%s" % version
    settings =  "os", "compiler", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    url = "http://github.com/lasote/conan-libiconv"

    exports="patches/*"

    def source(self):
        if not self.target_is_windows(): # wraps winiconv for windows
            zip_name = "libiconv-%s.tar.gz" % self.version
            download("http://ftp.gnu.org/pub/gnu/libiconv/%s" % zip_name, zip_name)
            check_md5(zip_name, "e34509b1623cec449dfeb73d7ce9c6c6")
            unzip(zip_name)
            os.unlink(zip_name)


    def configure(self):
        del self.settings.compiler.libcxx

        if self.target_is_windows():
            self.requires.add("winiconv/1.14.0@lasote/stable", private=False)
        
    def build(self):
        if self.target_is_windows():
            pass # wrapper for winiconv
        else:
            # apply patches in build to allow conditional change (ex.: different OSes)
            self.apply_patches()
            self.build_with_configure()
            
        
    def build_with_configure(self):
        autotools = AutoToolsBuildEnvironment(self)
        autotools.fpic=True

        args = [
            "--enable-static",
            "--enable-shared" ,
            "--disable-rpath"
        ]

        with tools.chdir(self.ZIP_FOLDER_NAME):
            self.run("pwd")
            self.run("ls")
            self.output.info("*******************************************************")

            autotools.configure(args=args)
            autotools.make()

    def package(self):
        self.copy("*.h", "include", path.join(self.ZIP_FOLDER_NAME, "include"), keep_path=True)

        if self.options.shared:
            self.copy(pattern="*.so*", dst="lib", src=self.ZIP_FOLDER_NAME, keep_path=False)
            self.copy(pattern="*.dll*", dst="bin", src=self.ZIP_FOLDER_NAME, keep_path=False)
        else:
            self.copy(pattern="*.a", dst="lib", src="%s" % self.ZIP_FOLDER_NAME, keep_path=False)
        
        self.copy(pattern="*.lib", dst="lib", src="%s" % self.ZIP_FOLDER_NAME, keep_path=False)
        
    def package_info(self):
        if not self.target_is_windows():
            self.cpp_info.libs = ['charset', 'iconv']

            if self.target_is_linux() or (self.options.shared and self.target_is_macos()):
                self.cpp_info.defines.append("LIBICONV_PLUG=1")

        else:
            self.cpp_info.includedirs = []

###################################################################################################

    def apply_patches(self):
        build_aux_root = path.join(self.ZIP_FOLDER_NAME, 'build-aux')
        build_aux_libcharset = path.join(self.ZIP_FOLDER_NAME, 'libcharset', 'build-aux')

        #update config.guess and config.sub to recognize Android
        for dest in [build_aux_root, build_aux_libcharset]:
            shutil.copy(path.join('patches', 'config.guess'), dest)
            shutil.copy(path.join('patches', 'config.sub'), dest)

        if self.target_is_linux() or self.target_is_android():
            stdio_in = os.path.join(self.ZIP_FOLDER_NAME, "srclib", "stdio.in.h")

            self.output.info('Applying patch in "{}" file'.format(stdio_in))

            text_to_replace = '_GL_WARN_ON_USE (gets, "gets is a security hole - use fgets instead");'
            replaced_text = '''
#if defined(__GLIBC__) && !defined(__UCLIBC__) && defined(__GLIBC_PREREQ)
#if !__GLIBC_PREREQ(2, 16)
_GL_WARN_ON_USE (gets, "gets is a security hole - use fgets instead");
#endif
#endif'''
            replace_in_file(stdio_in, text_to_replace, replaced_text)

    def target_is_android(self):
        return self.settings.os == "Android"
    def target_is_linux(self):
        return self.settings.os == "Linux"
    def target_is_macos(self):
        return self.settings.os == "Macos"
    def target_is_windows(self):
        return self.settings.os == "Windows"
