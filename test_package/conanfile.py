from conans import ConanFile, tools

import os
from os import path

class testConan(ConanFile):
    settings = "os", "compiler", "arch"

    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/testing"
    )

    generators = "Waf"
    exports = "wscript"

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")    # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.build_path = path.abspath("build")

        extra_env = {}

        if self.settings.arch == "x86" and self.settings.os != "Android":
            #Include this to be compatible with options in main conanfile
            extra_env["CFLAGS"] ="-m32"
            extra_env["LDFLAGS"]="-m32"

        with tools.environment_append(extra_env):
            self.run(
                "waf configure build -v -o %s" % (self.build_path),
                cwd=self.source_folder)

    def test(self):
        exec_path = path.join(self.build_path, 'example')
        self.output.info("running test: " + exec_path)
        self.run(exec_path)
